FROM phpdockerio/php74-fpm:latest as php

RUN apt-get update \
    && apt-get -y --no-install-recommends install php7.4-pdo php7.4-mysql php7.4-intl php7.4-soap php7.4-bcmath php7.4-gd cron \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

WORKDIR /app

COPY magento-cron /etc/cron.d/magento-cron

RUN chmod 0644 /etc/cron.d/magento-cron

RUN crontab /etc/cron.d/magento-cron

RUN chown www-data:www-data /app

USER www-data

# The 2 steps below allow benefiting from Docker layer cache when rebuilding without change to composer.json
#COPY --chown=www-data:www-data magento/composer.* auth.json /app/
#RUN composer install --no-interaction --no-dev -o --no-progress --no-suggest --apcu-autoloader

#RUN rm -f auth.json

COPY --chown=www-data:www-data src/ /app/

ENV MAGE_MODE=production

# Demo purpose only, your config.php file should already be present
# RUN bin/magento module:enable --all

RUN rm -f app/etc/env.php
RUN php -d memory_limit=1024M bin/magento setup:di:compile
RUN mv app/etc/config.php app/etc/config.php.orig
RUN mv app/etc/config.kube.php app/etc/config.php
    # You might specify themes to build using -t flag and add locales to build at the end (ie. en_US en_GB)
RUN php -d memory_limit=1024M bin/magento setup:static-content:deploy --jobs=$(nproc) --max-execution-time=3600
RUN mv app/etc/config.php.orig app/etc/config.php
RUN mv app/etc/env.kube.php app/etc/env.php

USER root

FROM nginx as nginx

COPY vhost.nginx /etc/nginx/conf.d/default.conf

COPY --from=php /app/pub /app/pub
