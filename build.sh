#!/bin/bash

# Exit on error
set -e

docker build -t conankid/magento-docker-fpm:2.4.2-3 --target php -f Dockerfile .
docker build -t conankid/magento-docker-nginx:2.4.2-3 --target nginx -f Dockerfile .

docker push conankid/magento-docker-fpm:2.4.2-3
docker push conankid/magento-docker-nginx:2.4.2-3